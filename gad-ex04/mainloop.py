from tkinter import *
import time
win = Tk() 
c = Canvas(win,width=300,height=200,bg="yellow",borderwidth=0,highlightthickness=0)
c.pack(expand=True,fill=BOTH) 
x   = 0
spd = 4 
dot = c.create_rectangle(x,95,x+10,105,fill="red",width=0)
while True:  
    c.moveto(dot,x,95)
    x+=spd
    if x >= c.winfo_width()-10 or x <= 0:
        spd*=-1
    c.update()
    time.sleep(0.002)
