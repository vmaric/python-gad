from tkinter import * 
win = Tk()  
c = Canvas(win,
width=300,
height=200,
borderwidth=0,
highlightthickness=0,
bg="yellow")
c.pack(expand=True,fill=BOTH) 
x = 0 
spd = 4 
red_box = c.create_rectangle(x,95,x+10,105,fill="red",width=0) 
def update(): 
    global x
    global spd
    win.after(2,update)
    c.moveto(red_box,x,95)
    x+=spd
    if x >= c.winfo_width()-10 or x <= 0:
        spd*=-1
win.after(2,update)
win.mainloop()