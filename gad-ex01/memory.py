import tkinter

window = tkinter.Tk("Memory Game")

stop_button         = tkinter.Button(window,text="Stop")
restart_button      = tkinter.Button(window,text="Restart")
leave_program       = tkinter.Button(window,text="Leave")

stop_button.grid(row=0,column=0,sticky="we")
restart_button.grid(row=0,column=1,sticky="we")
leave_program.grid(row=0,column=2,sticky="we")

counter = 1
for i in range(1,5):
    for j in range(3):
        tkinter.Button(window,text=str(counter)).grid(row=i,column=j,sticky="nesw")
        counter+=1

window.mainloop()