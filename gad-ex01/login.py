import tkinter

window = tkinter.Tk("Login form") 
window.geometry("400x150")

titleLabel          = tkinter.Label(window,text="User Login")
titleLabel.pack()

usernameLabel       = tkinter.Label(window,text="Username:")
usernameField       = tkinter.Entry(window) 
passwordLabel       = tkinter.Label(window,text="Password:")
passwordField       = tkinter.Entry(window)
btnLogin            = tkinter.Button(window,text="Login")
btnClose            = tkinter.Button(window,text="Close")

usernameLabel.pack(anchor="nw")
usernameField.pack(anchor="nw",fill="x")
passwordLabel.pack(anchor="nw")
passwordField.pack(anchor="nw",fill="x")
btnLogin.pack(expand=True,anchor="nw",fill="x",side="left")
btnClose.pack(expand=True,anchor="ne",fill="x",side="left")

window.mainloop()