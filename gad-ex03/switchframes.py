from tkinter import *

win = Tk()
win.title("Tk")

frame = None

def set_frame(fr):
    global frame
    if frame:
        frame.forget() 
    frame = fr(win)
    frame.pack(fill="both",expand=True)  

class MainFrame(Frame):
    def __init__(self, master=None, cnf={}, **kw):
        super().__init__(master,bg="red")
        self.bind("<Button-1>",lambda evt : set_frame(YellowFrame))  

class YellowFrame(Frame):
    def __init__(self, master=None, cnf={}, **kw):
        super().__init__(master,bg="yellow")
        self.bind("<Button-1>",lambda evt : set_frame(BlueFrame))  

class BlueFrame(Frame):
    def __init__(self, master=None, cnf={}, **kw):
        super().__init__(master,bg="blue")
        self.bind("<Button-1>",lambda evt : set_frame(MainFrame))  

set_frame(MainFrame) 
win.mainloop()
 