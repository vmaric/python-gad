from tkinter import *

VISA    = 1
MASTER  = 2
DINERS  = 3

win = Tk()

taxes = {
    VISA:1.01,
    DINERS:1.02,
    MASTER:1.03
}

def charge(tx):
    if txt_balance_entry.get().isnumeric:
        en = float(txt_balance_entry.get())
        val = en * taxes[tx]
        balance.set(f"{val:.2f}") 

balance = StringVar()

lbl_current_balance     = Label(win,text="Current balance:")
lbl_current_balance_val = Label(win,text="Current balance:", textvariable=balance)
txt_balance_entry       = Entry(win)
btn_visa                = Button(win,text="Visa",command=lambda:charge(VISA))
btn_master              = Button(win,text="Master",command=lambda:charge(MASTER))
btn_diners              = Button(win,text="Diners",command=lambda:charge(DINERS))

lbl_current_balance.grid(row=0,column=0,columnspan=2)
lbl_current_balance_val.grid(row=0,column=2)
txt_balance_entry.grid(row=1,column=0,columnspan=3)
btn_visa.grid(row=2,column=0,ipadx=5,ipady=5)
btn_master.grid(row=2,column=1,ipadx=5,ipady=5)
btn_diners.grid(row=2,column=2,ipadx=5,ipady=5)

win.mainloop()