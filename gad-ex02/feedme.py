from tkinter import *
import time, threading, random
win = Tk() 

title           = StringVar() 
wait_time       = 0
game_stopped    = False
hungry          = False

def feed():
    global hungry
    if not hungry:
        return
    now         = round(time.time() * 1000)
    wait        = round(wait_time * 1000)
    time_passed = (now - wait)
    if time_passed < 1000:
        title.set(f"Fed in {time_passed}")
        lbl.configure(fg="green")
        hungry = False
    else:
        title.set(f"Starved to death")
        lbl.configure(fg="red") 
        global game_stopped
        game_stopped = True

def heartbeat():
    while True: 
        time.sleep(random.randint(1,5))
        global game_stopped
        if game_stopped:
            break
        title.set("I am hungry")
        lbl.configure(fg="orange")
        global wait_time
        wait_time = time.time()
        global hungry
        hungry = True

lbl = Label(win,textvariable=title)
lbl.pack()

btn = Button(win,text="Feed me",width=20,height=5,command=feed)
btn.pack() 
threading.Thread(None,heartbeat).start()

win.mainloop()

