from tkinter import *

valid_users = {
    "peter":"123",
    "sally":"234",
    "john":"345"
}

def login_button_press():
    username = entry_username.get()
    password = entry_password.get()
    if not username in valid_users:
        msg.set("Invalid username")  
        entry_username.delete(0,END)
        entry_password.delete(0,END)
    else:
        if valid_users[username] == password:
            msg.set("Successfull login") 
        else:
            msg.set("Invalid password") 
            entry_password.delete(0,END)

window = Tk("Login form")
window.geometry("300x170")
window.resizable(False,False)

msg = StringVar()
msg.set("Login...")

Label(window,textvariable=msg).pack()
Label(window,text="Username: ").pack(anchor="nw") 
entry_username = Entry(window) 
entry_username.pack(anchor="nw",expand=True,fill="x")
Label(window,text="Password: ").pack(anchor="nw") 
entry_password = Entry(window,show="*")
entry_password.pack(anchor="nw",expand=True,fill="x") 
Button(window,text="Exit",command=window.quit).pack(fill="x",expand=True,side="left")
Button(window,text="Login",command=login_button_press).pack(anchor="n",fill="x",expand=True,side="right")

window.mainloop()